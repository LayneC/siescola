<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFrequenciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frequencia', function (Blueprint $table) {
            $table->timestamps();
            $table->bigIncrements('cdfrequencia');
            $table->bigInteger('cdmatdisciplina')->unsigned();
            $table->bigInteger('cdaula')->unsigned();
            $table->smallInteger('numero_faltas')->unsigned();


            $table->foreign('cdaula')->references('cdaula')->on('aula');
            $table->foreign('cdmatdisciplina')->references('cdmatdisciplina')->on('matdisciplina');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('frequencia');
    }
}
