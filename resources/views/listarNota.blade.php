@extends('adminlte::page')

@section('title', 'SIESCOLA - Nota')

@section('content_header')
@stop

@section('content')


<div id="line-one">
  <div class="container">
    <div class="row">
      <div class="col-md-12" id="center" style='text-align: center;'>              
        <h1><b>Notas</b></h1>
        <br>
      </div>             
    </div>
            
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('index')}}">Início</a></li>
        <li class="breadcrumb-item active" aria-current="page">Consultar</li>
      </ol>
    </nav>              

                    
    <div class="row">  
      <br>
      <h4 id="center" style='text-align: center;'><b>LISTA DAS NOTAS CADASTRADAS</b></h4>
      <br>              
    </div>

  
          <div class="box box-primary">
            <div class="box-header">
            @include('messages')
              <h3 class="box-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"></font></font></h3>
              </button>
              <form action="{{route('matdisciplina.search')}}" method="POST" class="form form-inline pull-right">
                {!! csrf_field() !!}
                <input type="text" name="nomedisciplina" class="form-control" placeholder="Nome Aluno">
                <button type="submit" class="btn btn-default">Pesquisar</button>
              </form>
              

            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <thead>
                    <tr>
                        <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit; position: center;">Aluno</font></font></th>
                        <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ação</font></font></th>
                    </tr>
                </thead>
                <tbody>
                @if (isset($listNota))
                  @foreach($listNota as $value)
                      <tr>
                          <th>{{$value->Aluno}}</th>
                          <th><a href="{{route('nota.show', $value->Matricula)}}" class="btn btn-primary"><i class="far fa-eye"></i></a>
                          <a href="" class="btn btn-warning"><i class="fas fa-pencil-alt"></i></a>
                            
                          </th>
                          
                      </tr>
                  @endforeach
                @endif
                
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    @stop