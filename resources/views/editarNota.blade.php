@extends('adminlte::page')

@section('title', 'SIESCOLA - Nota')

@section('content_header')
@stop

@section('content')
<div id="line-one">
  <div class="container">
    <div class="row">
      <div class="col-md-12" id="center" style='text-align: center;'>              
        <h1><b>Matriculas</b></h1>
        <br>
      </div>             
    </div>
            
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('index')}}">Início</a></li>
        <li class="breadcrumb-item active" aria-current="page">Cadastrar</li>
      </ol>
    </nav>              

                    
    <div class="row">  
      <br>
      <h4 id="center" style='text-align: center;'><b>EDIÇÃO DOS DADOS DA NOTA</b></h4>
      <br>              
    </div>
                
    <div class="box box-primary">
      
      <div class="box-body">
      @if ($errors->any())
        <div class="alert alert-warning">
          @foreach ($errors->all() as $error)
            <p>{{$error}}</p>
          @endforeach
        </div>
      @endif
        <form method="POST" action="{{ route('nota.update', $nota->cdnota)}}">
          @method('PATCH')
          @csrf
          @if(isset($nota) && !empty($nota->cdmatdisciplina))      
                    <input type="hidden"  name="cdmatdisciplina" value="{{ $nota->cdmatdisciplina }}">
                @else
                    <input type="hidden"  name="cdmatdisciplina" >
                @endif

          <div class="row">
            <div class="col-md-4">
                <label for="des">Nota:</label>
                @if(isset($nota) && !empty($nota->nota))      
                    <input type="text" class="form-control" name="nota" id="title" value="{{ $nota->nota }}">
                @else
                    <input type="text" class="form-control" name="nota" id="title">
                @endif
            </div>

            <div class="col-md-4">
                <label for="des">Referência:</label>
                @if(isset($nota) && !empty($nota->referencia))      
                    <input type="text" class="form-control" name="referencia" id="title" value="{{ $nota->referencia }}">
                @else
                    <input type="text" class="form-control" name="referencia" id="title">
                @endif
            </div>
            
            
          </div>
                  
          <div class="box-footer" style="margin-top: 4%;">
            <a href="{{route('nota.index')}}" class="btn btn-default">Cancelar</a>
            <button type="submit" class="btn btn-warning">Alterar</button>
          </div>
                      
        </form>
      </div>
    </div>             
  </div>
</div>
<!-- /.box-body -->
          
            
@stop