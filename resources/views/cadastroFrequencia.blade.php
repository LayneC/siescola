@extends('adminlte::page')

@section('title', 'SIESCOLA - Frequência')

@section('content_header')
@stop

@section('content')


<div id="line-one">
  <div class="container">
    <div class="row">
      <div class="col-md-12" id="center" style='text-align: center;'>              
        <h1><b>CADASTRO DE FREQUÊNCIA</b></h1>
        <br>
      </div>             
    </div>
            
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('index')}}">Início</a></li>
        <li class="breadcrumb-item active" aria-current="page">Consultar</li>
      </ol>
    </nav>              

                    
    <div class="row">  
      <br>
      <h4 id="center" style='text-align: center;'><b>CADASTRO DE FREQUÊNCIA</b></h4>
      <br>              
    </div>

  
    <div class="box box-primary">
    <div class="box-header">
        @if ($errors->any())
            <div class="alert alert-warning">
                @foreach ($errors->all() as $error)
                    <p>{{$error}}</p>
                @endforeach
            </div>
        @endif
        @include('messages')
        <h3 class="box-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"></font></font></h3>
        <a href="{{route('aula.index', ['$cddisciplina' => 'cddisciplina'])}}" class="btn btn-primary">Aulas Cadastradas</a>     
        
              

    </div>

    <!-- /.box-header -->

    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <form method="POST" action="{{ route('aula.store')}}">
                <div class="box-footer" style="margin-top: 4%;">
                    <a href="" class="btn btn-default">Cancelar</a>
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                </div>
                <div class="col-md-2">
                    <label><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Quantidade de aulas:</font></font></label>
                    <input type="text" name='numero_aulas' class="form-control" placeholder="">
                </div>
                <div class="col-md-2">
                    <label><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Data:</font></font></label>
                    <input type="date" name='data' class="form-control" placeholder="">
                </div>
                   
                <thead>
                    <tr>
                        <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit; position: center;">Aluno</font></font></th>
                        <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Número de Faltas:</font></font></th>
                    </tr>
                </thead>

                <tbody>
                    @if (isset($listAluDis))
                        @foreach($listAluDis as $value)
                            <tr>
                                <th>{{$value->Aluno}}</th>
                                <th>
                                    <div class="row">
                                            <input type="hidden" name="cdmatdisciplina[]" value="{{$value->Matdisciplina}}">                                                                                            
                                        <div class="col-md-2">
                                            <label><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"></font></font></label>
                                            <input type="text" name="numero_faltas[]" class="form-control" placeholder="">
                                        </div>                        
                                    </div>                                                   
                                </th>                        
                            </tr>
                        @endforeach
                        @csrf
                    @endif                      
                </tbody>
            </form>
        </table>
    </div>
            <!-- /.box-body -->
</div>
          <!-- /.box -->

@stop