@extends('adminlte::page')

@section('title', 'SIESCOLA - Perfil')

@section('content_header')
    
@stop

@section('content')
<div id="line-one">
    <div class="container">
        <div class="row">
            <div class="col-md-12" id="center" style='text-align: center;'>              
                <h1><b>Meu Perfil</b></h1>
                <br>
            </div>             
        </div>            

                
        <div class="box box-primary">
      
            <div class="box-body">
                @if ($errors->any())
                    <div class="alert alert-warning">
                        @foreach ($errors->all() as $error)
                            <p>{{$error}}</p>
                        @endforeach
                    </div>
                @endif
                @include('messages')

                <form method="POST" action="{{route('profile.update')}}">
                    @csrf
                    <div class="row">
                        <div class="col-md-8">
                            <label><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nome:</font></font></label>
                            <input type="text" value="{{ auth()->user()->name}}" name='name' class="form-control" placeholder="Nome">
                        </div>
                        <div class="col-md-8">
                            <label><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">E-mail:</font></font></label>
                            <input type="email" value="{{ auth()->user()->email}}" name='email' class="form-control" placeholder="E-mail">
                        </div>
                        <div class="col-md-8">
                            <label><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Senha:</font></font></label>
                            <input type="password"  name='password' class="form-control" placeholder="Nova Senha">
                        </div>
            
                    </div>
                  
                    <div class="box-footer" style="margin-top: 4%; margin-left: -0.5%;">
                        <button type="submit" class="btn btn-warning">Atualizar Perfil</button>
                    </div>
                      
                </form>
            </div>
        </div>             
    </div>
</div>



@stop