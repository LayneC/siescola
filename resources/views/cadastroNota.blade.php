@extends('adminlte::page')

@section('title', 'SIESCOLA - Nota')

@section('content_header')
@stop

@section('content')
<div id="line-one">
  <div class="container">
    <div class="row">
      <div class="col-md-12" id="center" style='text-align: center;'>              
        <h1><b>Nota</b></h1>
        <br>
      </div>             
    </div>
            
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('index')}}">Início</a></li>
        <li class="breadcrumb-item active" aria-current="page">Cadastrar</li>
      </ol>
    </nav>              

                    
    <div class="row">  
      <br>
      <h4 id="center" style='text-align: center;'><b>CADASTRO DOS DADOS DA NOTA</b></h4>
      <br>              
    </div>
                
    <div class="box box-primary">
      
      <div class="box-body">
      @if ($errors->any())
        <div class="alert alert-warning">
          @foreach ($errors->all() as $error)
            <p>{{$error}}</p>
          @endforeach
        </div>
      @endif
        <form method="POST" action="{{ route('nota.store')}}">
          @csrf
          <div class="row">
              <input type="hidden" name='cdmatdisciplina' value="{{$cdmat}}">
            
            <div class="col-md-2">
              <label><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nota:</font></font></label>
              <input type="text" name='nota' class="form-control" placeholder="Nota">
            </div>
            <div class="col-md-4">
              <label><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Referência:</font></font></label>
              <input type="text" name='referencia' class="form-control" placeholder="Referencia">
            </div>
            
          </div>
                  
          <div class="box-footer" style="margin-top: 4%;">
            <a href="{{route('matdisciplina.index')}}" class="btn btn-default">Cancelar</a>
            <button type="submit" class="btn btn-primary">Cadastrar</button>
          </div>
                      
        </form>
      </div>
    </div>             
  </div>
</div>
<!-- /.box-body -->
          
            
@stop