@if (session('success'))
    <div class = "alert alert-success">
        {{session('success')}}
    </div>
@endif

@if (session('errorDelete'))
    <div class = "alert alert-danger">
        {{session('errorDelete')}}
    </div>
@endif