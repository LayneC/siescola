@extends('adminlte::page')

@section('title', 'SIESCOLA - Matricula Disciplina')

@section('content_header')
@stop

@section('content')

<div id="line-one">
  <div class="container">
    <div class="row">
      <div class="col-md-12" id="center" style='text-align: center;'>              
        <h1><b>Matriculas em Disciplinas</b></h1>
        <br>
      </div>             
    </div>
            
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('index')}}">Início</a></li>
        <li class="breadcrumb-item active" aria-current="page">Consultar</li>
      </ol>
    </nav>              

                    
    <div class="row">  
      <br>
      <h4 id="center" style='text-align: center;'><b>LISTA DAS MATRICULAS EM DISCIPLINAS CADASTRADAS</b></h4>
      <br>              
    </div>

  
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"></font></font></h3>
              
              

            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
            
              @if ($errors->any())
                <div class="alert alert-warning">
                  @foreach ($errors->all() as $error)
                    <p>{{$error}}</p>
                  @endforeach
                </div>
              @endif

              <table class="table table-hover">
                <thead>
                    <tr>
                        <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit; position: center;">Disciplinas</font></font></th>
                        <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Adicionar Nota</font></font></th>
                        <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Notas Cadastradas</font></font></th>


                    </tr>
                </thead>
                <tbody >
                @if (isset($listAluDis))

                  @foreach($listAluDis as $value)
                      <tr>
                          <th>{{$value->Disciplina}}</th>
                          
                          <th>
                          <a href="{{route('nota.create', ['cdmatdisciplina' => $value->Matdisciplina])}}" class="btn btn-primary"><i class="fas fa-calculator"></i></a>
                          </th>
                          <th>
                          <a href="{{route('nota.show', $value->Matdisciplina)}}" class="btn btn-primary"><i class="fas fa-eye"></i></a>
                          </th>
                          
                      </tr>

                  @endforeach

                @endif
                
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    @stop