@extends('adminlte::page')

@section('title', 'SIESCOLA - Frequência')

@section('content_header')
@stop

@section('content')

<div id="line-one">
  <div class="container">
    <div class="row">
      <div class="col-md-12" id="center" style='text-align: center;'>              
        <h1><b>Frequência</b></h1>
        <br>
      </div>             
    </div>
            
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('index')}}">Início</a></li>
        <li class="breadcrumb-item active" aria-current="page">Consultar</li>
      </ol>
    </nav>              

                    
    <div class="row">  
      <br>
      <h4 id="center" style='text-align: center;'><b>LISTA DAS FREQUÊNCIAS CADASTRADAS</b></h4>
      <br>              
    </div>

  
          <div class="box box-primary">
            <div class="box-header">
            @include('messages')
            
              <h3 class="box-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"></font></font></h3>
              </button>
              

            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <thead>
                    <tr>
                        <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit; position: center;">Nome Aluno</font></font></th>
                        <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Numero de Faltas</font></font></th>
                        <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Editar</font></font></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($listAulaFreq as $value)
                    <tr>
                        <th>{{$value->Aluno}}</th>
                        <th>{{$value->NumFaltas}}</th>
                        <th>
                          <a href="{{route('frequencia.edit', ['cdfrequencia' => $value->cdfrequencia])}}" class="btn btn-warning"><i class="fas fa-pencil-alt"></i></a>
                        </th>
                        
                        
                    </tr>
                @endforeach
                
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    @stop