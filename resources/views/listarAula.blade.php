@extends('adminlte::page')

@section('title', 'SIESCOLA - Aula')

@section('content_header')
@stop

@section('content')

<div id="line-one">
  <div class="container">
    <div class="row">
      <div class="col-md-12" id="center" style='text-align: center;'>              
        <h1><b>Aulas</b></h1>
        <br>
      </div>             
    </div>
            
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('index')}}">Início</a></li>
        <li class="breadcrumb-item active" aria-current="page">Consultar</li>
      </ol>
    </nav>              

                    
    <div class="row">  
      <br>
      <h4 id="center" style='text-align: center;'><b>LISTA DAS AULAS CADASTRADAS</b></h4>
      <br>              
    </div>

  
          <div class="box box-primary">
            <div class="box-header">
            @include('messages')
            @if(isset($listDisProf))
                @foreach($listDisProf as $value)
            <div class="row"> 
                <h4 id="center" style='text-align: center;'><b>Disciplina: {{$value->NomeDis}}</b></h4>
                
                <h4 id="center" style='text-align: center;'><b>Professor responsável: {{$value->NomeProf}}</b></h4>
                <br>              
            </div>
                @endforeach
            @endif
              <h3 class="box-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"></font></font></h3>
              </button>

            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <thead>
                    <tr>
                        <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit; position: center;">Data</font></font></th>
                        <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Quantidade de Aulas</font></font></th>
                        <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Editar</font></font></th>
                        <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Frequência Cadastrada</font></font></th>

                    </tr>
                </thead>
                <tbody>
                @foreach($listAulaDis as $value)
                    <tr>
                        <th>{{$value->AulaData}}</th>
                        <th>{{$value->AulaNumero}}</th>
                        <th>
                          <a href="{{route('aula.edit', $value->cdaula, ['cddisciplina' => $value->cddisciplina])}}" class="btn btn-warning"><i class="fas fa-pencil-alt"></i></a>
                        </th>
                        <th>
                          <a href="{{route('frequencia.index', ['cdaula' => $value->cdaula])}}" class="btn btn-primary"><i class="fas fa-eye"></i></a>
                        </th>
                        
                    </tr>
                @endforeach
                
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    @stop