@extends('adminlte::page')

@section('title', 'SIESCOLA - Sobre')

@section('content_header')
@stop

@section('content')

<div id="line-one">
  <div class="container">
    <div class="row">
      <div class="col-md-12" id="center" style='text-align: center;'>              
        <h1><b>Sobre</b></h1>
        <br>
      </div>             
    </div>
            
              

                    
    <div class="row">  
      <br>
      <br>              
    </div>

  
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"></font></font></h3>
              </button>
             

            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <div style="padding: 4%;">
                <h3>Visão Geral: </h3>
                <h4>Sistema de gerenciamento de matrícula escolar, desenvolvido como requisito para a disciplina de Laboratório de Programação Web II, sob a orientação do professor Fábio Lima.</h4>
                <h3>Framework: </h3>
                <h4>Laravel</h4>
                <h3>Linguagens utilizadas: </h3>
                <h4>PHP e Javascript</h4>
                <h3>Desenvolvedora: </h3>
                <h4>Layne Castro</h4>
                <h3>Tempo de desenvolvimento: </h3>
                <h4>4 meses</h4>
              </div>
              
               
                
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    @stop