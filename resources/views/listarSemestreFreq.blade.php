@extends('adminlte::page')

@section('title', 'SIESCOLA - Frequência')

@section('content_header')
@stop

@section('content')

<div id="line-one">
  <div class="container">
    <div class="row">
      <div class="col-md-12" id="center" style='text-align: center;'>              
        <h1><b>Frequência</b></h1>
        <br>
      </div>             
    </div>
            
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('index')}}">Início</a></li>
        <li class="breadcrumb-item active" aria-current="page">Consultar</li>
      </ol>
    </nav>              

                    
    <div class="row">  
      <br>
      <h4 id="center" style='text-align: center;'><b>CADASTRO DE FREQUÊNCIA</b></h4>
      <br>              
    </div>

  
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"></font></font></h3>
              </button>
              

            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
            @if ($errors->any())
                <div class="alert alert-warning">
                  @foreach ($errors->all() as $error)
                    <p>{{$error}}</p>
                  @endforeach
                </div>
              @endif

              <table class="table table-hover">
                            <h5 style="margin-left:5%;"><b>Selecione o semestre atual:</b></h5>

                <thead>
                    <tr>
                        <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit; position: center;"></font></font></th>
                        <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"></font></font></th>
                        

                    </tr>
                </thead>
                <tbody>
                
                @foreach($list_Semestre as $value)
                    <tr>
                        <th>{{$value->ano}}</th>
                        
                          <th>
                            <a href="{{route('aulacreate.create', ['cddisciplina' => request()->get('cddisciplina'), 'cdsemestre' => $value->cdsemestre])}}" class="btn btn-primary">Prosseguir</a>                        
                          </th>

                        
                    </tr>
                @endforeach
                
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    @stop