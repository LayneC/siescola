<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
    //
    protected $table = 'nota';
    protected $primaryKey = 'cdnota';

    protected $fillable = [
        'cdnota',
        'cdmatdisciplina',
        'nota',
        'referencia',
    ];

    public function matdisciplina()
    {
        return $this->belongsTo('App\matdisciplina', 'cdmatdisciplina');
    }

}
