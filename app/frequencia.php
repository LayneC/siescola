<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class frequencia extends Model
{
    //
    protected $table = 'frequencia';
    protected $primaryKey = 'cdfrequencia';

    protected $fillable = [
        'cdfrequencia',
        'cdmatdisciplina',
        'cdaula',
        'numero_faltas',
    ];

    public function matdisciplina()
    {
        return $this->belongsTo('App\matdisciplina', 'cdmatdisciplina');
    }

    public function aula()
    {
        return $this->belongsTo('App\aula', 'cdaula');
    }
}
