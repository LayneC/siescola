<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class aula extends Model
{
    //
    protected $table = 'aula';
    protected $primaryKey = 'cdaula';

    protected $fillable = [
        'cdaula',
        'data',
        'numero_aulas',
    ];
}
