<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AlunoValidationFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        
        return true;
        
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nmatricula' => 'required|unique:aluno|int|min:7',
            'nome' => 'required|string',
            'status' => 'required',
            'cpf' => 'required|min:11|unique:aluno|regex:/^\d{3}\.\d{3}\.\d{3}\-\d{2}$/',

        ];
    }

    public function messages()
    {
        return[
            'nmatricula.unique' => 'O número de matricula já está cadastrado!',
            'nmatricula.required' => 'O campo número de matricula é obrigatório!',
            'nmatricula.int' => 'Insira apenas números no campo número de matricula!',
            'nome.required' => 'O campo nome é obrigatório!',
            'status.required' => 'O campo status é obrigatório!',

            
        ];

    }
}
