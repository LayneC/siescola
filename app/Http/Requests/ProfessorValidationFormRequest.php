<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfessorValidationFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|string',
            'nciape' => 'required|int|unique:professor',

        ];
    }

    public function messages()
    {
        return[
            'nciape.required' => 'O número SIAPE é obrigatório!',
            'nciape.unique' => 'O número SIAPE inserido já está cadastrado!'
        ];

    }
}
