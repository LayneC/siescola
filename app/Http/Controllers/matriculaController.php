<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\matricula;
use App\aluno;
use App\curso;
use App\semestre;
use App\turma;
use App\disciplina;
use App\matdisciplina;
use App\Http\Requests\MatriculaValidationFormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;



class matriculaController extends Controller
{

    public function index()
    {
        $listMatricula = matricula::orderBy('cdmatricula')->get();
        $listMatdisciplina = \DB::table('aluno')
                                ->join('matricula', 'aluno.cdaluno', '=', 'matricula.cdaluno')
                                ->join('curso', 'matricula.cdcurso', '=', 'curso.cdcurso')
                                ->join('disciplina', 'curso.cdcurso', '=', 'disciplina.cdcurso')
                                ->join('semestre', 'matricula.cdsemestre', '=', 'semestre.cdsemestre')
                                ->join('turma', 'matricula.cdturma', '=', 'turma.cdturma')
                                ->select('aluno.nome as Aluno', 'curso.nomecurso as Curso', 'semestre.ano as Semestre', 'turma.nometurma as Turma', 'matricula.statusMat as Status','matricula.cdmatricula as Matricula')->distinct()
                                ->get();
        return view('listarMatricula', compact('listMatricula', 'listMatdisciplina'));
    }

    
    public function create()
    {

        $data = [
            'aluno' => aluno::all(),
            'curso' => curso::all(),
            'semestre' => semestre::all(),
            'turma' => turma::all(),
            'matricula' => matricula::all()
        ];
        //$data['aluno'];

        return view('cadastroMatricula', compact ('data'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $cdaluno = $request->cdaluno;
        $cdsemestre = $request->cdsemestre;

        $request->validate([
            'cdcurso' => 'required',
            'cdsemestre' => 'required',
            'cdturma' => 'required',
            'statusMat' => 'required',
            'cdaluno' => ['required', Rule::unique('matricula')->where(function ($query) use ($cdaluno, $cdsemestre)
            {
                return $query->where('cdaluno', $cdaluno)->where('cdsemestre', $cdsemestre);
            }),
        ]
        ],
        [
            'cdaluno.unique' => 'O aluno selecionado já está matriculado no semestre informado! '
        ]);

        $lastid=matricula::create($data)->cdmatricula;


    if(!is_null($request->cddisciplina))
        {
        foreach($request->cddisciplina as $item=>$v){
            $data2=array(
                'cdmatricula'=>$lastid,
                'cddisciplina'=>$request->cddisciplina[$item],
            );
        matdisciplina::create($data2);
        }
        
    }
        
    return redirect('/matricula')->with(['success' => 'Matricula cadastrada com sucesso!']);

    }

    public function show($id)
    {
        $listMatricula = matricula::orderBy('cdmatricula')->get();
        $listMatdisciplina = \DB::table('aluno')
                                ->join('matricula', 'aluno.cdaluno', '=', 'matricula.cdaluno')
                                ->join('matdisciplina', 'matricula.cdmatricula', '=', 'matdisciplina.cdmatricula')
                                ->join('curso', 'matricula.cdcurso', '=', 'curso.cdcurso')
                                ->join('semestre', 'matricula.cdsemestre', '=', 'semestre.cdsemestre')
                                ->join('turma', 'matricula.cdturma', '=', 'turma.cdturma')
                                ->select('aluno.nome as Aluno', 'curso.nomecurso as Curso', 'semestre.ano as Semestre', 'turma.nometurma as Turma', 'matricula.statusMat as Status','matricula.cdmatricula as Matricula')->distinct()
                                ->get();
        return view('listarMatricula', compact('listMatricula', 'listMatdisciplina'));
    }

    public function edit($cdmatricula)
    {
        $matricula = matricula::findOrFail($cdmatricula);
        $data = [
            'aluno' => aluno::all(),
            'curso' => curso::all(),
            'semestre' => semestre::all(),
            'turma' => turma::all()
        ];

        return view('editMatricula', compact ('data', 'matricula'));

        //return view('editMatricula', ['matricula' => $matricula], ['data']);

    }

    public function update(Request $request, $cdmatricula){

        $data = $request->all();

        $validator = Validator::make($data, [
            'cdcurso' => 'required',
            'cdsemestre' => 'required',
            'cdturma' => 'required',
            'statusMat' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        $matricula = matricula::findOrFail($cdmatricula);
        $matricula->update($request->all());
        return redirect()->route('matricula.show', $matricula->cdmatricula);
    }

    public function searchMatricula(Request $request, matricula $matricula)
    {
        $data = $request->all();

        $listMatricula = $matricula->where(function($query) use ($data, $matricula) {

                if (isset($data['nome'])){
                    $cdaluno = $matricula->get_cdaluno_by_name($data['nome']);
                    $query->whereIn('cdaluno', $cdaluno);
                }
            }
        )->get();  

        return view('listarMatricula', compact('listMatricula'));
        
    }
}
