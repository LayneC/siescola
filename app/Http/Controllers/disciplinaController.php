<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\disciplina;
use App\professor;
use App\curso;
use App\Http\Requests\DisciplinaValidationFormRequest;
use Illuminate\Support\Facades\Input;



class disciplinaController extends Controller
{
    public function index()
    {
        $listDisciplina = disciplina::orderBy('nomedisciplina')->get();
        return view('listarDisciplina', compact('listDisciplina'));
    }

    // public function disciplinasByCurso(Request $request) {
        //$cdcurso = $request->query('curso_id');
        //$disciplinas = disciplina::where('cdcurso', $cdcurso)->get();

        //$html = '';
        //$disciplinas = disciplina::where('cdcurso', $cdcurso)->get();
        //foreach ($disciplinas as $disciplina) {
        //    $html .= '<option value="'.$disciplina->cddisciplina.'">'.$disciplina->nomedisciplina.'</option>';
        //}
        //return response()->json(['html' => $html]);
    //}

    public function disciplinas(Request $request){

        $curso_id = $request->query('cdcurso');
        $disciplinas = disciplina::where('cdcurso', '=', $curso_id)->get();
        return response()->json($disciplinas);
    }

    public function create()
    {
        $disciplinas_prof = professor::all();
        $disciplinas_curso = curso::all();

        return view('cadastroDisciplina', compact ('disciplinas_prof', 'disciplinas_curso'));
    }

    public function store(DisciplinaValidationFormRequest $request)
    {
        $disciplina = disciplina::create($request->all());
        return view('listarDisciplina', ['listDisciplina' => disciplina::orderBy('nomedisciplina')->get()]);

    }

    public function show($id)
    {
        $listDisciplina = disciplina::orderBy('nomedisciplina')->get();
        return view('listarDisciplina', compact('listDisciplina'));
    }

    public function edit($cddisciplina)
    {
        $disciplina = disciplina::findOrFail($cddisciplina);
        $disciplinas_prof = professor::all();
        return view('editDisciplina', ['disciplina' => $disciplina], ['disciplinas_prof' => $disciplinas_prof]);

    }

    public function update(DisciplinaValidationFormRequest $request, $cddisciplina){
        $disciplina = disciplina::findOrFail($cddisciplina);
        $disciplina->update($request->all());
        return redirect()->route('disciplina.show', $disciplina->cddisciplina);
    }

    public function searchDisciplina(Request $request, disciplina $disciplina)
    {
        $nomedisciplina = $request->input('nomedisciplina');
        $data = $request->all();

        $listDisciplina = $disciplina->where(function($query) use ($data, $disciplina) {
                if (isset($data['nomedisciplina']))
                    $query->where('nomedisciplina', 'LIKE', '%' . $data['nomedisciplina'] . '%');

                if (isset($data['nome'])){
                    $cdprofessor = $disciplina->get_cdprofessor_by_name($data['nome']);
                    $query->whereIn('cdprofessor', $cdprofessor);
                }
            }
        )->get();  

       //$listDisciplina = disciplina::where('nomedisciplina', 'LIKE', "%$nomedisciplina%")->get();

        return view('listarDisciplina', compact('listDisciplina'));
        
    }
}
