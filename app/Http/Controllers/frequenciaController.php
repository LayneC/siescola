<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\frequencia;
use App\matdisciplina;
use App\disciplina;
use App\matricula;
use App\aluno;
use App\aula;
use Illuminate\Support\Facades\Validator;



class frequenciaController extends Controller
{
    //
   public function index(Request $request)
   {

    $cdAula = $request->query('cdaula');

    $listAulaFreq = \DB::table('aluno')
    ->join('matricula', 'aluno.cdaluno', '=', 'matricula.cdaluno')
    ->join('matdisciplina', 'matricula.cdmatricula', '=', 'matdisciplina.cdmatricula')
    ->join('frequencia', 'matdisciplina.cdmatdisciplina', '=', 'frequencia.cdmatdisciplina')
    ->join('aula', 'frequencia.cdaula', '=', 'aula.cdaula')
    ->where('aula.cdaula', $cdAula)
    ->select('aluno.nome as Aluno', 'frequencia.numero_faltas as NumFaltas', 'frequencia.cdfrequencia as cdfrequencia')
    ->get();

   

    

    return view('listarFrequencia', compact('cdAula', 'listAulaFreq'));

   }
   

   public function edit($cdfrequencia)
    {
        $frequencia = frequencia::findOrFail($cdfrequencia);
        

        return view('editFrequencia', compact ( 'frequencia'));

        //return view('editMatricula', ['matricula' => $matricula], ['data']);

    }

    public function update(Request $request, $cdfrequencia){
        $frequencia = frequencia::findOrFail($cdfrequencia);
        $frequencia->update($request->all());
        return redirect('/disciplina')->with(['success' => 'Frequência editada com sucesso!']);
    }

    
}
