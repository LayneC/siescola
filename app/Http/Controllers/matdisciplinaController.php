<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\matdisciplina;
use App\matricula;
use App\disciplina;
use App\aluno;
use App\curso;
use App\Http\Requests\MatdisciplinaValidationFormRequest;
use Alert;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;



class matdisciplinaController extends Controller
{
    public function index(){
        $listMatdisciplina = \DB::table('aluno')
                                ->join('matricula', 'aluno.cdaluno', '=', 'matricula.cdaluno')
                                ->join('matdisciplina', 'matricula.cdmatricula', '=', 'matdisciplina.cdmatricula')
                                ->select('aluno.nome as Aluno', 'matricula.cdmatricula as Matricula')->distinct()
                                ->get();

        return view('listarMatdisciplina', compact('listMatdisciplina'));
    }
    public function create()
    {

        $queryAluMat = \DB::table('matricula')
                            ->join('aluno', 'matricula.cdaluno', '=', 'aluno.cdaluno')
                            ->select('aluno.nome as Aluno', 'matricula.cdmatricula as Matricula')
                            ->get();
        

        $data = [
            'matricula' => matricula::all(),
            'disciplina' => disciplina::all(),
            'matdisciplina' => matdisciplina::all()
        ];
        //$data['aluno'];

        return view('cadastroMatdisciplina', compact ('queryAluMat', 'data'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $cdmatricula = $request->cdmatricula;
        $cddisciplina = $request->cddisciplina;

        //dd(matdisciplina::where('cdmatricula', $cdmatricula)->where('cddisciplina', $cddisciplina)->get());
        $request->validate([
            'cdmatricula' => 'required',
            'cddisciplina' => ['required', Rule::unique('matdisciplina')->where(function ($query) use ($cdmatricula, $cddisciplina)
            {
                return $query->where('cdmatricula', $cdmatricula)->where('cddisciplina', $cddisciplina);
            }),
        ]
        ],
        [
            'cddisciplina.unique' => 'Disciplina já cadastrada para o aluno selecionado!'
        ]);
        $matdisciplina = matdisciplina::create($request->all());

        return redirect()->route('matdisciplina.index');

    }

    public function show($cdmatricula)
    {
        $matricula = matricula::findOrFail($cdmatricula);

        $listAluDis = \DB::table('disciplina')
                                ->join('matdisciplina', 'disciplina.cddisciplina', '=', 'matdisciplina.cddisciplina')
                                ->join('matricula', 'matdisciplina.cdmatricula', '=', 'matricula.cdmatricula')
                                ->where('matricula.cdmatricula', $cdmatricula)
                                ->select('disciplina.nomedisciplina as Disciplina', 'matricula.cdmatricula as Matricula', 'matdisciplina.cdmatdisciplina as Matdisciplina')                             
                                ->get();

        return view('listarAlunoDisciplina', compact('listAluDis', 'matricula'));

        
    }

    public function edit($cdmatricula)
    {
        $matricula = matricula::findOrFail($cdmatricula);

        $data = [
            'matricula' => matricula::all(),
            'disciplina' => disciplina::all(),
            'matdisciplina' => matdisciplina::all()
        ];

        return view('editMatdisciplina', compact ('data', 'matricula'));

        //return view('editMatricula', ['matricula' => $matricula], ['data']);

    }

    public function update(MatdisciplinaValidationFormRequest $request, $cdmatdisciplina){
        $matdisciplina = matdisciplina::findOrFail($cdmatdisciplina);
        $matdisciplina->update($request->all());
        return redirect()->route('matdisciplina.show', $matdisciplina->cdmatdisciplina);
    }

    public function searchMatricula(Request $request, matricula $matricula)
    {
        $data = $request->all();

        $listMatricula = $matricula->where(function($query) use ($data, $matricula) {

                if (isset($data['nome'])){
                    $cdaluno = $matricula->get_cdaluno_by_name($data['nome']);
                    $query->whereIn('cdaluno', $cdaluno);
                }
            }
        )->get();  

        return view('listarMatdisciplina', compact('listMatricula'));
        
    }

    public function destroy ($cdmatdisciplina) 
    {

        $aluDis = matdisciplina::findOrFail($cdmatdisciplina);


        $disNota = \DB::table('matdisciplina')
        ->join('nota', 'matdisciplina.cdmatdisciplina', '=', 'nota.cdmatdisciplina')
        ->where('matdisciplina.cdmatdisciplina', $aluDis)
        ->select('matdisciplina.cdmatdisciplina')                             
        ->get();

        $disFreq = \DB::table('matdisciplina')
        ->join('frequencia', 'matdisciplina.cdmatdisciplina', '=', 'frequencia.cdmatdisciplina')
        ->where('matdisciplina.cdmatdisciplina', $aluDis)
        ->select('matdisciplina.cdmatdisciplina')                             
        ->get();

        if((is_null($disNota)) || (is_null($disFreq))){
            $delete = $aluDis->delete();
            
            if ($delete){

            return redirect()
                    ->route('matricula.index')
                    ->with(['success' => 'Disciplina deletada com sucesso!']);
            }else{
                return redirect()->route('matricula.index')->with(['errorDelete' => 'Erro ao deletar!']);
            }
        }
        


        
    }

    public function aludisCreate(Request $request)
    {
       

        $cdmatricula = $request->query('cdmatricula');
        $disciplina = \DB::table('matricula')
        ->join('curso', 'matricula.cdcurso', '=', 'curso.cdcurso')
        ->join('disciplina', 'curso.cdcurso', '=', 'disciplina.cdcurso')
        ->where('matricula.cdmatricula', $cdmatricula)
        ->select('disciplina.nomedisciplina as nomeDisciplina', 'disciplina.cddisciplina as cddisciplina', 'matricula.cdmatricula as Matricula')                             
        ->get();

        $data = $request->all();
        //$data['aluno'];

        return view('cadastroAluDis', compact ('cdmatricula', 'data', 'disciplina'));
    }

   
    public function aludisStore(Request $request)
    {

        $data = $request->all();
        $cdmatricula = $request->cdmatricula;
        $cddisciplina = $request->cddisciplina;


        

        $request->validate([
            'cdmatricula' => 'required',
            'cddisciplina' => ['required', Rule::unique('matdisciplina')->where(function ($query) use ($cdmatricula, $cddisciplina)
            {
                return $query->where('cdmatricula', $cdmatricula)->where('cddisciplina', $cddisciplina);
            }),
        ]
        ],
        [
            'cddisciplina.unique' => 'Disciplina já cadastrada para o aluno selecionado!'
        ]);

        $matricula = matricula::find($data['cdmatricula']);

        Matdisciplina::create($data);

        return redirect('/matricula')->with(['success' => 'Disciplina adicionada com sucesso!']);



    }
}
