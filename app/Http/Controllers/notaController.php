<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nota;
use App\matdisciplina;
use App\matricula;
use App\disciplina;
use Illuminate\Support\Facades\Validator;



class notaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listNota = \DB::table('aluno')
                                ->join('matricula', 'aluno.cdaluno', '=', 'matricula.cdaluno')
                                ->join('matdisciplina', 'matricula.cdmatricula', '=', 'matdisciplina.cdmatricula')
                                ->join('nota', 'matdisciplina.cdmatdisciplina', '=', 'nota.cdmatdisciplina')
                                ->select('aluno.nome as Aluno', 'matricula.cdmatricula as Matricula')->distinct()
                                ->get();

        return view('listarNota', compact('listNota'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       $cdmat = $request->query('cdmatdisciplina');

       return view('cadastroNota', compact('cdmat'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();

        $validator = Validator::make($data, [
            'cdmatdisciplina' => 'required|exists:matdisciplina,cdmatdisciplina',
            'nota' => 'required|numeric|min:0|max:10',
            'status' => 'in:AT,IN',
            'referencia' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        $matdisciplina = matdisciplina::find($data['cdmatdisciplina']);
        Nota::create($data);

        return redirect('/matricula')->with(['success' => 'Nota cadastrada com sucesso!']);


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Nota  $nota
     * @return \Illuminate\Http\Response
     */
    public function show($cdmatdisciplina)
    {
        $matdisciplina = matdisciplina::findOrFail($cdmatdisciplina);

        $listNota = \DB::table('matdisciplina')
                                ->join('nota', 'matdisciplina.cdmatdisciplina', '=', 'nota.cdmatdisciplina')
                                ->join('matricula', 'matdisciplina.cdmatricula', '=', 'matricula.cdmatricula')
                                ->join('disciplina', 'matdisciplina.cddisciplina', '=', 'disciplina.cddisciplina')
                                ->where('matdisciplina.cdmatdisciplina', $cdmatdisciplina)
                                ->select( 'nota.cdnota as Cdnota', 'nota.nota as Nota', 'nota.referencia as Referencia', 'matricula.cdmatricula as Matricula', 'matdisciplina.cdmatdisciplina')  
                                ->orderby('disciplina.nomedisciplina', 'ASC')
                                ->get();

                                $listNotaMedia = \DB::table('matdisciplina')
                                ->join('nota', 'matdisciplina.cdmatdisciplina', '=', 'nota.cdmatdisciplina')
                                ->join('matricula', 'matdisciplina.cdmatricula', '=', 'matricula.cdmatricula')
                                ->join('disciplina', 'matdisciplina.cddisciplina', '=', 'disciplina.cddisciplina')
                                ->where('matdisciplina.cdmatdisciplina', $cdmatdisciplina)
                                ->select( array('nota.cdnota as Cdnota', 'nota.nota as Nota', 'nota.referencia as Referencia', 'matricula.cdmatricula as Matricula', 'matdisciplina.cdmatdisciplina',
                                        \DB::raw('avg(nota.nota) as media')))  
                                ->groupBy('matdisciplina.cdmatdisciplina')         
                                //->orderby('disciplina.nomedisciplina', 'ASC')
                                ->get();

        return view('listarAlunoNota', compact('matdisciplina','listNota', 'listNotaMedia'));

        
    }

   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Nota  $nota
     * @return \Illuminate\Http\Response
     */
    public function edit($cdnota)
    {
        //
        $nota = Nota::find($cdnota);

        
        return view('editarNota', ['nota' => $nota]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Nota  $nota
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cdnota)
    {
        //
        $data = $request->all();

        $validator = Validator::make($data, [
            'cdmatdisciplina' => 'required|exists:matdisciplina,cdmatdisciplina',
            'nota' => 'required|numeric|min:0|max:10',
            'referencia' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        $nota = nota::findOrFail($cdnota);
        $nota->update($request->all());
        return redirect('/matdisciplina')->with(['success' => 'Nota editada com sucesso!']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Nota  $nota
     * @return \Illuminate\Http\Response
     */
    public function destroy(Nota $nota)
    {
        //
    }

}
