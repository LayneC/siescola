<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\frequencia;
use App\matdisciplina;
use App\disciplina;
use App\matricula;
use App\aluno;
use App\aula;
use App\professor;
use App\semestre;
use App\Rules\NumFaltaMax;
use Illuminate\Support\Facades\Validator;

class aulaController extends Controller
{
    //
    public function index(Request $request)
    {

        $cddis = $request->query('cddisciplina');

       $listAulaDis = \DB::table('disciplina')
       ->join('matdisciplina', 'disciplina.cddisciplina', '=', 'matdisciplina.cddisciplina')
       ->join('frequencia', 'matdisciplina.cdmatdisciplina', '=', 'frequencia.cdmatdisciplina')
       ->join('aula', 'frequencia.cdaula', '=', 'aula.cdaula')
       ->where('matdisciplina.cddisciplina', $cddis)
       ->select('aula.data as AulaData', 'aula.numero_aulas as AulaNumero', 'aula.cdaula as cdaula', 'disciplina.cddisciplina as cddisciplina')->distinct()                        
       ->get();

       $listDisProf = \DB::table('professor')
       ->join('disciplina', 'professor.cdprofessor', '=', 'disciplina.cdprofessor')
       ->join('matdisciplina', 'disciplina.cddisciplina', '=', 'matdisciplina.cddisciplina')
       ->join('frequencia', 'matdisciplina.cdmatdisciplina', '=', 'frequencia.cdmatdisciplina')
       ->join('aula', 'frequencia.cdaula', '=', 'aula.cdaula')
       ->where('matdisciplina.cddisciplina', $cddis)
       ->select('disciplina.nomedisciplina as NomeDis', 'professor.nome as NomeProf')->distinct()                          
       ->get();
        
        return view('listarAula',  compact('listAulaDis', 'listDisProf'));

    }

    public function create($cddisciplina, $cdsemestre)
    {
        
       $listAluDis = \DB::table('disciplina')
       ->join('matdisciplina', 'disciplina.cddisciplina', '=', 'matdisciplina.cddisciplina')
       ->join('matricula', 'matdisciplina.cdmatricula', '=', 'matricula.cdmatricula')
       ->join('semestre', 'matricula.cdsemestre', '=', 'semestre.cdsemestre')
       ->join('aluno', 'matricula.cdaluno', '=', 'aluno.cdaluno')
       ->where('matdisciplina.cddisciplina', $cddisciplina)
       ->where('matricula.cdsemestre', $cdsemestre)
       ->select('aluno.nome as Aluno', 'matricula.cdmatricula as Matricula', 'matdisciplina.cdmatdisciplina as Matdisciplina')                          
       ->get();

       $listMatDis = \DB::table('disciplina')
       ->join('matdisciplina', 'disciplina.cddisciplina', '=', 'matdisciplina.cddisciplina')
       ->join('matricula', 'matdisciplina.cdmatricula', '=', 'matricula.cdmatricula')
       ->join('aluno', 'matricula.cdaluno', '=', 'aluno.cdaluno')
       ->where('matdisciplina.cddisciplina', $cddisciplina)
       ->select('matdisciplina.cdmatdisciplina as Matdisciplina')                         
       ->get();

       return view('cadastroFrequencia', compact( 'listAluDis', 'listMatDis'));

    }

    public function semestreCreate(Request $request){
        $list_Semestre = semestre::orderBy('ano')->get();
        $cddis = $request->query('cddisciplina');

        return view('listarSemestreFreq', compact('list_Semestre', 'cddis'));
    }

    public function store(Request $request)
    {
        //
        $data = $request->all();
        $numeroAulas = $request->numero_aulas;

        $validator = Validator::make($data, [
            'data' => 'required',
            'numero_aulas' => 'required|int|min:1|max:12',
            'numero_faltas.*' => ['required', 'min:0', new NumFaltaMax($numeroAulas)],
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }


        if(!is_null($request->cdmatdisciplina))
        {
            $lastid=aula::create($data)->cdaula;

            foreach($request->cdmatdisciplina as $item=>$v){
                $data2=array(
                    'cdaula'=>$lastid,
                    'cdmatdisciplina'=>$request->cdmatdisciplina[$item],
                    'numero_faltas' =>$request->numero_faltas[$item],
                );
            $aulacreate = frequencia::create($data2);
            }
            return redirect('/disciplina/show')->with(['success' => 'Frequência cadastrada com sucesso!']);

        }

       
        return redirect('/disciplina/show')->with(['errorDelete' => 'Frequência não cadastrada, nenhum aluno participante da disciplina!']);



        

    }

    public function edit($cdaula)
    {
        $aula = aula::findOrFail($cdaula);
        

        return view('editAula', compact ( 'aula'));

        //return view('editMatricula', ['matricula' => $matricula], ['data']);

    }

    public function update(Request $request, $cdaula){
        $aula = aula::findOrFail($cdaula);
        $aula->update($request->all());
        return redirect('/disciplina')->with(['success' => 'Aula editada com sucesso!']);
    }
}
