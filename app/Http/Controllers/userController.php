<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UpdateProfileFormRequest;


class userController extends Controller
{
    //
    public function profile()
    {
        return view ('editPerfil');
    }

    public function profileupdate(UpdateProfileFormRequest $request)
    {
        $data = $request->all();

        if ($data['password'] != null)
            $data['password'] = bcrypt($data['password']);
        else
            unset($data['password']);

        $update = auth()->user()->update($data);

        if ($update)
            return redirect()
                        ->route('profile')
                        ->with('success', 'Perfil alterado com sucesso!');

        return redirect()
                    ->back()
                    ->with('error', 'Falha ao atualizar o perfil!');
        
    }
}
