<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NumFaltaMax implements Rule
{
    private $numeroAulas;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($numeroAulas)
    {
        $this->numeroAulas = $numeroAulas;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $value <= $this->numeroAulas;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'O número de faltas não pode ser superior ao número de aulas.';
    }
}
